/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Role;
import model.User;

/**
 *
 * @author PC
 */
public class UserDAO extends DBContext {

    public User getAllInfoByEmail(String email) {
        String sql = "SELECT [user_id]\n"
                + "      ,[userName]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[phone]\n"
                + "      ,[gender]\n"
                + "      ,[fullName]\n"
                + "      ,[school]\n"
                + "      ,[facebook]\n"
                + "      ,[twitter]\n"
                + "      ,[instagram]\n"
                + "      ,[description]\n"
                + "      ,[created_at]\n"
                + "      ,[role_id]\n"
                + "  FROM [SWP391_G5].[dbo].[user]"
                + "where email = ? ";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setGender(rs.getInt(6));
                user.setFullName(rs.getString(7));
                user.setSchool(rs.getString(8));
                user.setFacebook(rs.getString(9));
                user.setTwitter(rs.getString(10));
                user.setInstagram(rs.getString(11));
                user.setDescription(rs.getString(12));
                user.setCreate_at(rs.getDate(13));

                Role role = new Role();
                role.setRole_id(rs.getInt(14));
                user.setRole(role);
                return user;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    public List<User> getAllUserByROle(int role) {
        List<User> list = new ArrayList<>();
        String sql = """
                     SELECT *    FROM [SWP391_G5].[dbo].[user] where role_id = ? """;

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
           ps.setInt(1, role);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setGender(rs.getInt(6));
                user.setFullName(rs.getString(7));
                user.setSchool(rs.getString(8));
                user.setFacebook(rs.getString(9));
                user.setTwitter(rs.getString(10));
                user.setInstagram(rs.getString(11));
                user.setDescription(rs.getString(12));
                user.setCreate_at(rs.getDate(13));

                Role roles = new Role();
                roles.setRole_id(rs.getInt(14));
                user.setRole(roles);
                 list.add(user);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void updateProfile(String email, String userName, String fulName, String phone, String school, String facebook, String twitter, String instagram) {
        String sql = "UPDATE [dbo].[user]\n"
                + "   SET [userName] = ?\n,"
                + "    [phone] = ?\n,"
                + "    [fullName] = ?\n,"
                + "    [school] = ?\n,"
                + "    [facebook] = ?\n,"
                + "    [twitter] = ?\n,"
                + "    [instagram] = ?\n"
                + " WHERE email = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, userName);
            ps.setString(2, phone);
            ps.setString(3, fulName);
            ps.setString(4, school);
            ps.setString(5, facebook);
            ps.setString(6, twitter);
            ps.setString(7, instagram);
            ps.setString(8, email);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public User getById(int userId) {
        String sql = "SELECT [user_id], [userName], [email], [password], [phone], [gender], "
                + "[fullName], [school], [facebook], [twitter], [instagram], [description], "
                + "[created_at], [role_id] "
                + "FROM [SWP391_G5].[dbo].[user] "
                + "WHERE [user_id] = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setUser_id(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4));
                user.setPhone(rs.getString(5));
                user.setGender(rs.getInt(6));
                user.setFullName(rs.getString(7));
                user.setSchool(rs.getString(8));
                user.setFacebook(rs.getString(9));
                user.setTwitter(rs.getString(10));
                user.setInstagram(rs.getString(11));
                user.setDescription(rs.getString(12));
                user.setCreate_at(rs.getDate(13));

                Role role = new Role();
                role.setRole_id(rs.getInt(14));
                user.setRole(role);

                return user;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        UserDAO d = new UserDAO();
//        d.updateProfile("namhamss2003@gmail.com", "Admin", "Honag Xuan Nam 01", "0123456789", "FPT", "", "", "");
           System.out.println(d.getAllUserByROle(2));
    }
}
